""" Argos3 gym environment and utility functions
"""
import threading
import socket
import subprocess
import os
import sys
import platform
import shutil
from time import sleep
import logging
import psutil
import sh
import numpy as np
import gym
from gym import spaces

LOGGER = logging.getLogger('Argos3Env')
streamhandle = logging.StreamHandler(sys.stderr)
streamhandle.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamhandle.setFormatter(formatter)

LOGGER.addHandler(streamhandle)

class Argos3Env(gym.Env):
    """A base class for environments using ARGoS3
    Implements the gym.Env interface. See
    https://gym.openai.com/docs
    and
    https://github.com/openai/gym/tree/master/gym/envs#how-to-create-new-environments-for-gym
    Based on the UnityEnv class in the rl-unity repo (see github).
    """
    metadata = {'render.modes': ['human']}

    def __init__(self, width, height, additional_params):
        """ Initializes everything.
        """
        self.proc = None
        self.soc = None
        self.connected = False

        self.width = width
        self.height = height
        self.log_argos3 = True
        self.logfile = None
        self.restart = False
        self.set_params(additional_params)

    def set_params(self, params):
        """ Some additional parameters
        """
        self.robots_num = params['num_robots']
        self.action_dim = params['action_size']
        self.state_dim = params['state_size']
        self.data_type = params['data_type']
        if self.data_type == "numerical":
            self.buffer_size = self.state_dim * 4 # 4 bytes to a float
        else:
            self.frame_dim = 2000000000 # big enough
            self.buffer_size = self.frame_dim + self.state_dim
        self.action_space = spaces.Box(
            -1 * np.ones(self.action_dim),
            1 * np.ones(self.action_dim))
            #params['min_speed'] * np.ones(self.action_dim),
            #params['max_speed'] * np.ones(self.action_dim))


    def conf(self, loglevel='INFO', log_argos3=True, logfile=None, *args, **kwargs):
        """ Configures the logger.
        """
        LOGGER.setLevel(getattr(logging, loglevel.upper()))
        self.log_argos3 = log_argos3
        if logfile:
            self.logfile = open(logfile, 'w')

    def connect(self):
        """ Connects to localhost, displays the path
        to the simulator binary.
        """
        self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = '127.0.0.1'
        port = get_free_port(host)
        LOGGER.debug('Port: {}'.format(port))
        assert port != 0
        LOGGER.debug(f"Platform {platform.platform()}")
        pl = 'unix'
        bin = sh.which('argos3')
        env = os.environ.copy()

        env.update(ARGOS_PORT=str(port))

        env.update(DATA=self.data_type)

        LOGGER.debug(f'Simulator binary {bin}')

        def stdw():
            """ Takes whatever the subprocess is emiting
            and pushes it to the standard output.
            """
            for c in iter(lambda: self.proc.stdout.read(1), ''):
                sys.stdout.write(c)
                sys.stdout.flush()

        def poll():
            """ Limits the memory used by the subprocess.
            """
            while not self.proc.poll():
                limit = 3
                if self.memory_usage(self.proc.pid) > limit * 1024**3:
                    LOGGER.warning(f'Memory usage above {limit}gb. Restarting after this episode.')
                    self.restart = True
                sleep(5)
            LOGGER.debug(f'Simulator returned with {self.proc.returncode}')

            config_dir = os.path.expanduser('~/.config/argos3/argos3-th') # which means that's where you have to put the config files
            if os.path.isdir(config_dir):
                shutil.rmtree(config_dir, ignore_errors=True)

        def limit():
            """ Limits resources used. Only limits the
            address space by default.
            """
            l = 6 * 1024**3 #for 3gb of address space. Works for unity so should be more than enough.
            try:
                # set whatever limits you want in this block
                pass
            except Exception as e:
                print(e)
                raise

        """The following configures stderr, launches a subprocess,
        begins a thread and establishes a connection to the simulator.
        """
        stderr = self.logfile if self.logfile else (subprocess.PIPE if self.log_argos3 else subprocess.DEVNULL)
        self.proc = subprocess.Popen([bin, '-c', 'argos3-th/argos/treasure_hunt.argos'],
                                      env=env,
                                      stdout=stderr,
                                      universal_newlines=True,
                                      preexec_fn=limit)

        threading.Thread(target=poll, daemon=True).start()

        print('Subprocess opened')
        print(f'Host: {host}, port: {port}')

        #threading.Thread(target=stdw, daemon=True).start()

        # wait until connection with simulator process
        timeout = 20
        for i in range(timeout * 10):
          if self.proc.poll():
            LOGGER.debug('simulator died')
            break

          try:
            self.soc.connect((host, port))
            LOGGER.debug('trying to connect')
            self.soc.settimeout(20*60) # 20 minutes
            self.connected = True
            LOGGER.debug(f'sockname : {self.soc.getsockname()}')
            LOGGER.debug('finally connected')
            break
          except ConnectionRefusedError as e:
            if i == timeout * 10 - 1:
              print(e)

          sleep(.1)

        if not self.connected:
          raise ConnectionRefusedError('Connection with simulator could not be established.')

    def reset(self):
        if self.restart:
            self.disconnect()
            self.restart = False
        if not self.connected:
            self.connect()

        self.send(np.zeros(self.action_dim), reset=True)
        state, frame = self.receive()

        return state, frame

    def receive(self):
        """ Receive data from simulator process.
        """
        LOGGER.debug("receiving from simulator")
        data_in = b""
        chunk = None
        while len(data_in) < self.buffer_size and chunk != b'':
            chunk = self.soc.recv(min(1024, self.buffer_size - len(data_in)))
            data_in += chunk

        # if not looking at frames
        if self.data_type is "numerical":
            state = np.frombuffer(data_in, np.float32, self.state_dim, 0)
            frame = None
        else:
            # convert frame pixels to numpy array
            # 80x80 frame (6400 pixels)
            bytes_per_line = 80
            byte_number = bytes_per_line**2
            frame = np.frombuffer(data_in, np.uint8, byte_number, 0)
            bytes_per_col = byte_number/bytes_per_line
            frame = np.reshape(frame, [bytes_per_line, bytes_per_col])
            frame = frame[::-1, :, :3]
            state = None
            LOGGER.debug("Frame received")

        self.last_state = state
        self.last_frame = frame

        return state, frame

    def send(self, action, reset=False):
        """ Send action to execute through socket.
        """
        act = np.concatenate((action, [1. if reset else 0.]))
        #act = np.concatenate(([-0.1], [1. if reset else 0.]))
        act = np.array(act, dtype=np.float32)
        assert act.shape == (self.action_dim + 1,)
        #assert act.shape == (self.action_dim,)

        data_out = act.tobytes()
        self.soc.sendall(data_out)

    def disconnect(self):
        """ Disconnect everything.
        """
        if self.proc:
            self.proc.kill()
        if self.soc:
            self.soc.close()
        self.connected = False

    def _close(self):
        """ Close subprocess, socket and logfile.
        """
        LOGGER.debug('close')
        if self.proc:
            self.proc.kill()
        if self.soc:
            self.soc.close()
        if self.logfile:
            self.logfile.close()

    def _render(self, mode='human', *args, **kwargs):
        pass

    def memory_usage(self, pid):
        proc = psutil.Process(pid)
        mem = proc.memory_info().rss #resident memory
        for child in proc.children(recursive=True):
            try:
                mem += child.memory_info().rss
            except psutil.NoSuchProcess:
                pass
        return mem

def get_free_port(host):
    """As the name indicates, get a port.
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((host, 0))
    port = sock.getsockname()[1]
    sock.close()
    return port

